﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ToDoAPI.Models
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var conventer = new BoolToZeroOneConverter<byte>();
            modelBuilder
                .Entity<TodoItem>()
                .Property(t => t.IsComplete)
                .HasConversion(conventer);
        }

        public DbSet<TodoItem> Todos { get; set; }
    }
}
